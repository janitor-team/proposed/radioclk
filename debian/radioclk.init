#! /bin/sh
#
### BEGIN INIT INFO
# Provides:          radioclk
# Required-Start:    $local_fs $remote_fs $network
# Required-Stop:     $local_fs $remote_fs
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Start simple radio NTP reference clock daemon
# Description: Start simple radio NTP reference clock daemon
### END INIT INFO

. /lib/lsb/init-functions

PATH=/sbin:/bin:/usr/sbin:/usr/bin
DAEMON=/usr/sbin/radioclkd
NAME=radioclkd
DESC="Radio Clock daemon"
DEFAULTS=/etc/default/radioclk
PIDFILE=/var/run/$NAME.pid

test -x $DAEMON || exit 5

set -e

if [ -e "$DEFAULTS" ] ; then
	. "$DEFAULTS"
fi

if [ "$PORT" = "" ] ; then
	printf "You need to edit $DEFAULTS before using $NAME.\n"
	exit 0
fi

case "$1" in
  start)
	echo -n "Starting $DESC: "
	start-stop-daemon --start --quiet --pidfile $PIDFILE --exec $DAEMON -- $PORT
	echo "$NAME."
	;;
  stop)
	echo -n "Stopping $DESC: "
	start-stop-daemon --oknodo --stop --quiet --pidfile $PIDFILE --exec $DAEMON
	echo "$NAME."
	;;
  #reload)
	#
	#	If the daemon can reload its config files on the fly
	#	for example by sending it SIGHUP, do it here.
	#
	#	If the daemon responds to changes in its config file
	#	directly anyway, make this a do-nothing entry.
	#
	# echo "Reloading $DESC configuration files."
	# start-stop-daemon --stop --signal 1 --quiet --pidfile \
	#	/var/run/$NAME.pid --exec $DAEMON
  #;;
  restart|force-reload)
	#
	#	If the "reload" option is implemented, move the "force-reload"
	#	option to the "reload" entry above. If not, "force-reload" is
	#	just the same as "restart".
	#
	echo -n "Restarting $DESC: "
	start-stop-daemon --stop --quiet --pidfile $PIDFILE --exec $DAEMON
	sleep 1
	start-stop-daemon --start --quiet --pidfile $PIDFILE --exec $DAEMON -- $PORT
	echo "$NAME."
	;;
  status)
	status_of_proc -p $PIDFILE $DAEMON $NAME && exit 0 || exit $?
	;;
  *)
	N=/etc/init.d/$NAME
	# echo "Usage: $N {start|stop|restart|reload|force-reload}" >&2
	echo "Usage: $N {start|stop|restart|force-reload}" >&2
	exit 1
	;;
esac

exit 0
